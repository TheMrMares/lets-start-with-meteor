import React from 'react';
import { FlowRouter } from 'meteor/kadira:flow-router'
import { mount } from 'react-mounter'

import Layout from '../imports/ui/layout'
import Home from '../imports/ui/pages/Home'
import Notes from '../imports/ui/pages/Notes'

FlowRouter.route('/', {
  name: 'Home',
  action(){
    mount( Layout, {
      content: Home
    })
  }
})

FlowRouter.route('/notes', {
  name: 'Notes',
  action(){
    mount( Layout, {
      content: Notes
    })
  }
})

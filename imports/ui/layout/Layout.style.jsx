export default ({ palette }) => ({
    myButton: {
        border: `3px solid ${palette.secondary.main}`,
    },
    wrapper: {
      border: '1px solid red',
      display: 'flex',
      flexDirection: 'column',
    }
})

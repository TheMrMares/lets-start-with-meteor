import React from "react";
import PropTypes from "prop-types";
import Button from "@material-ui/core/Button";
import { withStyles } from "@material-ui/core/styles";
import withMuiTheme from "../theme/withMuiTheme";
import style from "./Layout.style";

import Header from './Header'
import Footer from './Footer'

export class Layout extends React.Component {
  static propTypes = {
    content: PropTypes.any,
    classes: PropTypes.object
  };

  render() {
    const { classes, content: Content } = this.props;
    return (
      <div className={classes.wrapper}>
        <Header />
        <Content />
        <Footer />
      </div>
    );
  }
}

export default withMuiTheme(withStyles(style)(Layout), true);

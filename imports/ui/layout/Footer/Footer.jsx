import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import style from './Footer.style'

export class Footer extends React.Component {
  static propTypes = {
    classes: PropTypes.object,
  }
  render() {
    const { classes } = this.props
    return (
      <div className={classes.wrapper}>
        Footer
      </div>
    )
  }
}

export default withStyles(style)(Footer)

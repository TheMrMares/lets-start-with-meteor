import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import style from './Navigation.style'

import FlowLink from '../../../Components/FlowLink'

export class Navigation extends React.Component {
  static propTypes = {
    classes: PropTypes.object,
  }

  constructor() {
    super()

    this.state = {
      navigationItems: [
        { name: 'Home', to: '/' },
        { name: 'Notes', to: '/notes' }
      ]
    }
  }

  render() {
    const { classes } = this.props
    const { navigationItems } = this.state
    return (
      <div className={classes.root}>
        {navigationItems.map(item => (
          <FlowLink className={classes.navItem} key={item.name} to={item.to}>
            {item.name}
          </FlowLink>
        ))}
      </div>
    )
  }
}

export default withStyles(style)(Navigation)

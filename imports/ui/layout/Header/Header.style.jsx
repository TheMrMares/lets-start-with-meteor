export default () => ({
  root: {
    border: '1px solid blue',
  },
  toolbar: {
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'space-between',
  }
})

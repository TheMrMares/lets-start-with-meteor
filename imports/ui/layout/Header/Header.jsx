import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import style from './Header.style'

import FlowLink from '../../Components/FlowLink'
import Navigation from './Navigation'

export class Header extends React.Component {
  static propTypes = {
    classes: PropTypes.object,
  }
    render() {
      const { classes } = this.props
      return (
        <div className={classes.root}>
          <AppBar
            className={classes.appbar}
            position="sticky"
          >
            <Toolbar className={classes.toolbar} disableGutters>
              <FlowLink to="/">
                Homepage
              </FlowLink>
              <Navigation />
            </Toolbar>
          </AppBar>
        </div>
      )
    }
}

export default withStyles(style)(Header)

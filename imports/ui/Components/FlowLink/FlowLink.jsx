import { FlowRouter } from 'meteor/kadira:flow-router'
import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { withStyles } from '@material-ui/core/styles'
import style from './FlowLink.style'

export class FlowLink extends React.Component {
  static propTypes = {
    classes: PropTypes.object,
    className: PropTypes.string,
    children: PropTypes.oneOfType([PropTypes.node, PropTypes.func]),
    to: PropTypes.string,
  }

  handleClick = evt => {
    const { to } = this.props
    evt.preventDefault()
    FlowRouter.go(to)
  }

  render() {
    const { classes, className, children } = this.props
    return (
      <a className={classNames(className, classes.root)} onClick={this.handleClick}>
        {children}
      </a>
    )
  }
}

export default withStyles(style)(FlowLink)
